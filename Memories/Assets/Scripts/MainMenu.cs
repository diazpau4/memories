using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject MainMenuu, Creditss;

    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Jugar()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void Credits()
    {
        MainMenuu.SetActive(false);
        Creditss.SetActive(true);
    }

    public void Back()
    {
        MainMenuu.SetActive(true);
        Creditss.SetActive(false);
    }
    public void Salir()
    {
        Application.Quit();
    }
}
