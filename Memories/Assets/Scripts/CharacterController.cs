using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float rapidezDesplazamiento = 5.0f;
    public Camera camaraPrimeraPersona;
    public Transform Hand;
    public GameObject Object,
                      Door;
    public float fuerza;
    private Vector3 Escala;
    private bool EnMano;
    
    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Escala = Object.transform.localScale;
         
        
    }

    
    void Update()
    {
        PickingUpandUnpicking();
        Movement();

      
    }

   


    public void Movement()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime; movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }

    public void PickingUpandUnpicking()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.transform.gameObject.CompareTag("Agarrable") && hit.distance <= 5)
            {
                Object = hit.transform.gameObject;
                Object.transform.SetParent(Hand);
                Object.transform.position = Hand.position;
                Object.transform.rotation = Hand.rotation;
                Object.GetComponent<Rigidbody>().isKinematic = true;
                EnMano = true;
            }
        }

        if(Input.GetMouseButtonDown(1))
        {
            Object.transform.SetParent(null);
            Object.GetComponent<Rigidbody>().isKinematic = false;
            Object.transform.localScale = Escala;
            if (EnMano == true)
            {
                Object.GetComponent<Rigidbody>().AddForce(transform.forward * fuerza, ForceMode.Impulse);
                EnMano = false;
            }
        }


        if (Input.GetMouseButtonUp(0))
        {

            Object.transform.SetParent(null);
            Object.GetComponent<Rigidbody>().isKinematic = false;
            Object.transform.localScale = Escala;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "DActivator")
        {
            Door.SetActive(false);
        }

        if (other.tag == "Activator")
        {
            Door.SetActive(true);
        }
    }
}
