using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideosManagerAct : MonoBehaviour
{
    public GameObject pantalla;
    public GameObject personaje;
    public GameObject Pelota, LetraP, LetraE, LetraT, PantallaInicial, ParedInicial, PantallaFinal, ParedFinal, PelotaBasket, Cubos, Placas;


    void Start()
    {

    }


    void Update()
    {
        VideoInicial();
        VideoFinal();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (personaje.tag == "character")
        {
            pantalla.GetComponent<VideoPlayer>().enabled = true;
            Cubos.SetActive(true);
            Placas.SetActive(true);
        }

        if (Pelota.tag == "Agarrable")
        {
            pantalla.GetComponent<VideoPlayer>().enabled = true;
            Pelota.GetComponent<BoxCollider>().enabled = false;
            LetraP.SetActive(true);
            LetraE.SetActive(true);
            LetraT.SetActive(true);
        }

    }

    public void VideoInicial()
    {
        if (PantallaInicial != null && PantallaFinal != null)
        {
            if (PantallaInicial.GetComponent<VideoPlayer>().isPaused)
            {
                PantallaInicial.SetActive(false);
                ParedInicial.SetActive(false);
            }
        }
    }


    public void VideoFinal()
    {
        if (PantallaFinal != null)
        {
            if (PantallaFinal.GetComponent<VideoPlayer>().isPaused)
            {
                SceneManager.LoadScene("Main Menu");
            }
        }
    }
}
