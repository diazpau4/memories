using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class LvlManager : MonoBehaviour
{
    public static LvlManager instance;
    bool bossOpen = false;
    public Material Green,
                    Red,
                    LightBlue;
    public int Nv1,
               Nv2,
               Nv3,
               Nv4,
               Nv5,
               Nv6,
               Nv7,
               Nv8,
               Nv9,
               Nv10,
               Nv11,
               Nv12,
               Nv13,
               Nv14,
               Nv15,
               Nv16,
               Nv17,
               Nv18,
               Nv19,
               Nv20,
               Nv21;

    public GameObject LightLine1LV1, LightLine2LV1, LightLine3LV1, LightLine4LV1, LightLine5LV1, LightLine6LV1, LightLine7LV1, Frame, PhotosL1,
                      LightLine1LV2, LightLine2LV2, LightLine3LV2, LightLine4LV2, LightLine5LV2, LightLine6LV2, LightLine7LV2, Frame2, PhotosL2,
                      LightLine1LV3, LightLine2LV3, LightLine3LV3, LightLine4LV3, LightLine5LV3, LightLine6LV3, LightLine7LV3, Frame3, PhotosL3,
                      LightLine1LV4, LightLine2LV4, LightLine3LV4, LightLine4LV4, LightLine5LV4, LightLine6LV4, LightLine7LV4, Frame4, PhotosL4,
                      LightLine1LV5, LightLine2LV5, LightLine3LV5, LightLine4LV5, LightLine5LV5, LightLine6LV5, LightLine7LV5, Frame5, PhotosL5,
                      LightLine1LV6, LightLine2LV6, LightLine3LV6, LightLine4LV6, LightLine5LV6, LightLine6LV6, LightLine7LV6, Frame6, PhotosL6,
                      LightLine1LV7, LightLine2LV7, LightLine3LV7, LightLine4LV7, LightLine5LV7, LightLine6LV7, LightLine7LV7, LightLine8LV7, Frame7, PhotosL7,
                      LightLine1LV8, LightLine2LV8, LightLine3LV8, LightLine4LV8, LightLine5LV8, LightLine6LV8, LightLine7LV8, Frame8, PhotosL8,
                      LightLine1LV9, LightLine2LV9, LightLine3LV9, LightLine4LV9, LightLine5LV9, LightLine6LV9, LightLine7LV9, Frame9, PhotosL9,
                      LightLine1LV10, LightLine2LV10, LightLine3LV10, LightLine4LV10, LightLine5LV10, LightLine6LV10, LightLine7LV10, LightLine8LV10, Frame10, PhotosL10;



    public GameObject pantalla1,
                      pantalla2,
                      wp1,
                      wp2,
                      wp3,
                      Vidrios,
                      VidriosEstrellas,
                      VidriosSaturno,
                      VidriosLuna,
                      VidriosEarth,
                      VidriosK,
                      FinalDoor,
                      PlacaFinal, PlacaLVL4, Placa2LVL4, PlacaDesLVL4;


    public void Awake()
    {          
        if (instance == null)
        {
            instance = this;
        }


    }

    void Start()
    {
      
    }

    public void Lvl1(bool EsPalabra)
    {
      
        if (EsPalabra)
        {
            Nv1++;
        }
        else
        {
            Nv1--;
        }
    }
    public void Lvl2(bool EsPalabra2)
    {
        if (EsPalabra2)
        {
            Nv2++;
        }
        else
        {
            Nv2--;
        }
    }

    public void Lvl3(bool EsPalabra3)
    {
        if (EsPalabra3)
        {
            Nv3++;
        }
        else
        {
            Nv3--;
        }
    }

    public void Lvl4(bool EsPalabra4)
    {
        if (EsPalabra4)
        {
            Nv4++;
        }
        else
        {
            Nv4--;
        }
    }

    public void Lvl5(bool EsPalabra5)
    {
        if (EsPalabra5)
        {
            Nv5++;
        }
        else
        {
            Nv5--;
        }
    }

    public void Lvl6(bool EsPalabra6)
    {
        if (EsPalabra6)
        {
            Nv6++;
        }
        else
        {
            Nv6--;
        }
    }

    public void Lvl7(bool EsPalabra7)
    {
        if (EsPalabra7)
        {
            Nv7++;
        }
        else
        {
            Nv7--;
        }


    }

    public void Lvl8(bool EsPalabra8)
    {
        if (EsPalabra8)
        {
            Nv8++;
        }
        else
        {
            Nv8--;
        }
    }

    public void Lvl9(bool EsPalabra9)
    {
        if (EsPalabra9)
        {
            Nv9++;
        }
        else
        {
            Nv9--;
        }
    }

    public void Lvl10(bool EsPalabra10)
    {
        if (EsPalabra10)
        {
            Nv10++;
        }
        else
        {
            Nv10--;
        }
    }


    public void Update()
    {
        if (Nv1 == 3)
        {
            LightLine1LV1.GetComponent<Renderer>().material = Green;
            LightLine2LV1.GetComponent<Renderer>().material = Green;
            LightLine3LV1.GetComponent<Renderer>().material = Green;
            LightLine4LV1.GetComponent<Renderer>().material = Green;
            LightLine5LV1.GetComponent<Renderer>().material = Green;
            LightLine6LV1.GetComponent<Renderer>().material = Green;
            LightLine7LV1.GetComponent<Renderer>().material = Green;    
            PhotosL1.SetActive(true);
            Frame.GetComponent<Renderer>().material = Green;


        }
        else if (Nv1 < 3)
        {
            LightLine1LV1.GetComponent<Renderer>().material = Red;
            LightLine2LV1.GetComponent<Renderer>().material = Red;
            LightLine3LV1.GetComponent<Renderer>().material = Red;
            LightLine4LV1.GetComponent<Renderer>().material = Red;
            LightLine5LV1.GetComponent<Renderer>().material = Red;
            LightLine6LV1.GetComponent<Renderer>().material = Red;
            LightLine7LV1.GetComponent<Renderer>().material = Red;
            PhotosL1.SetActive(false);
            Frame.GetComponent<Renderer>().material = LightBlue;
        }

        if (Nv2 == 5)
        {
            LightLine1LV2.GetComponent<Renderer>().material = Green;
            LightLine2LV2.GetComponent<Renderer>().material = Green;
            LightLine3LV2.GetComponent<Renderer>().material = Green;
            LightLine4LV2.GetComponent<Renderer>().material = Green;
            LightLine5LV2.GetComponent<Renderer>().material = Green;
            LightLine6LV2.GetComponent<Renderer>().material = Green;
            LightLine7LV2.GetComponent<Renderer>().material = Green;
            PhotosL2.SetActive(true);
            Frame2.GetComponent<Renderer>().material = Green;
        }
        else if (Nv2 < 5)
        {
            LightLine1LV2.GetComponent<Renderer>().material = Red;
            LightLine2LV2.GetComponent<Renderer>().material = Red;
            LightLine3LV2.GetComponent<Renderer>().material = Red;
            LightLine4LV2.GetComponent<Renderer>().material = Red;
            LightLine5LV2.GetComponent<Renderer>().material = Red;
            LightLine6LV2.GetComponent<Renderer>().material = Red;
            LightLine7LV2.GetComponent<Renderer>().material = Red;
            PhotosL2.SetActive(false);
            Frame2.GetComponent<Renderer>().material = LightBlue;
        }
        if (Nv3 == 2)
        {
            wp1.SetActive(true);
            wp2.SetActive(true);
            wp3.SetActive(true);
            VidriosK.SetActive(false);
        }

        if (Nv3 == 5)
        {
            LightLine1LV3.GetComponent<Renderer>().material = Green;
            LightLine2LV3.GetComponent<Renderer>().material = Green;
            LightLine3LV3.GetComponent<Renderer>().material = Green;
            LightLine4LV3.GetComponent<Renderer>().material = Green;
            LightLine5LV3.GetComponent<Renderer>().material = Green;
            LightLine6LV3.GetComponent<Renderer>().material = Green;
            LightLine7LV3.GetComponent<Renderer>().material = Green;
            PhotosL3.SetActive(true);
            Frame3.GetComponent<Renderer>().material = Green;
        }
        else if (Nv3 < 5)
        {
            LightLine1LV3.GetComponent<Renderer>().material = Red;
            LightLine2LV3.GetComponent<Renderer>().material = Red;
            LightLine3LV3.GetComponent<Renderer>().material = Red;
            LightLine4LV3.GetComponent<Renderer>().material = Red;
            LightLine5LV3.GetComponent<Renderer>().material = Red;
            LightLine6LV3.GetComponent<Renderer>().material = Red;
            LightLine7LV3.GetComponent<Renderer>().material = Red;
            PhotosL3.SetActive(false);
            Frame3.GetComponent<Renderer>().material = LightBlue;
        }
        if (Nv4 == 3)
        {
            Vidrios.SetActive(false);
            pantalla2.SetActive(true);
            pantalla1.SetActive(false);
            PlacaLVL4.SetActive(false);
            Placa2LVL4.SetActive(true);
            PlacaDesLVL4.SetActive(true);
            



        }
        else if (Nv4 == 8)
        {
            LightLine1LV4.GetComponent<Renderer>().material = Green;
            LightLine2LV4.GetComponent<Renderer>().material = Green;
            LightLine3LV4.GetComponent<Renderer>().material = Green;
            LightLine4LV4.GetComponent<Renderer>().material = Green;
            LightLine5LV4.GetComponent<Renderer>().material = Green;
            LightLine6LV4.GetComponent<Renderer>().material = Green;
            LightLine7LV4.GetComponent<Renderer>().material = Green;
            PhotosL4.SetActive(true);
            Frame4.GetComponent<Renderer>().material = Green;
            pantalla2.GetComponent<VideoPlayer>().enabled = false;
            pantalla1.SetActive(true);
            PlacaLVL4.SetActive(true);
            Placa2LVL4.SetActive(false);
            PlacaDesLVL4.SetActive(false);

        }
        else if (Nv4 < 8)
        {
            LightLine1LV4.GetComponent<Renderer>().material = Red;
            LightLine2LV4.GetComponent<Renderer>().material = Red;
            LightLine3LV4.GetComponent<Renderer>().material = Red;
            LightLine4LV4.GetComponent<Renderer>().material = Red;
            LightLine5LV4.GetComponent<Renderer>().material = Red;
            LightLine6LV4.GetComponent<Renderer>().material = Red;
            LightLine7LV4.GetComponent<Renderer>().material = Red;
            PhotosL4.SetActive(false);
            Frame4.GetComponent<Renderer>().material = LightBlue;

        }

        if (Nv5 == 6)
        {
            LightLine1LV5.GetComponent<Renderer>().material = Green;
            LightLine2LV5.GetComponent<Renderer>().material = Green;
            LightLine3LV5.GetComponent<Renderer>().material = Green;
            LightLine4LV5.GetComponent<Renderer>().material = Green;
            LightLine5LV5.GetComponent<Renderer>().material = Green;
            LightLine6LV5.GetComponent<Renderer>().material = Green;
            LightLine7LV5.GetComponent<Renderer>().material = Green;
            PhotosL5.SetActive(true);
            Frame5.GetComponent<Renderer>().material = Green;
        }
        else if (Nv5 < 6)
        {
            LightLine1LV5.GetComponent<Renderer>().material = Red;
            LightLine2LV5.GetComponent<Renderer>().material = Red;
            LightLine3LV5.GetComponent<Renderer>().material = Red;
            LightLine4LV5.GetComponent<Renderer>().material = Red;
            LightLine5LV5.GetComponent<Renderer>().material = Red;
            LightLine6LV5.GetComponent<Renderer>().material = Red;
            LightLine7LV5.GetComponent<Renderer>().material = Red;
            PhotosL5.SetActive(false);
            Frame5.GetComponent<Renderer>().material = LightBlue;
        }

        if (Nv6 == 7)
        {
            LightLine1LV6.GetComponent<Renderer>().material = Green;
            LightLine2LV6.GetComponent<Renderer>().material = Green;
            LightLine3LV6.GetComponent<Renderer>().material = Green;
            LightLine4LV6.GetComponent<Renderer>().material = Green;
            LightLine5LV6.GetComponent<Renderer>().material = Green;
            LightLine6LV6.GetComponent<Renderer>().material = Green;
            LightLine7LV6.GetComponent<Renderer>().material = Green;
            PhotosL6.SetActive(true);
            Frame6.GetComponent<Renderer>().material = Green;
        }
        else if (Nv6 < 7)
        {
            LightLine1LV6.GetComponent<Renderer>().material = Red;
            LightLine2LV6.GetComponent<Renderer>().material = Red;
            LightLine3LV6.GetComponent<Renderer>().material = Red;
            LightLine4LV6.GetComponent<Renderer>().material = Red;
            LightLine5LV6.GetComponent<Renderer>().material = Red;
            LightLine6LV6.GetComponent<Renderer>().material = Red;
            LightLine7LV6.GetComponent<Renderer>().material = Red;
            PhotosL6.SetActive(false);
            Frame6.GetComponent<Renderer>().material = LightBlue;
        }

        if (Nv7 == 2)
        {
            LightLine1LV7.GetComponent<Renderer>().material = Green;
            LightLine2LV7.GetComponent<Renderer>().material = Green;
            LightLine3LV7.GetComponent<Renderer>().material = Green;
            LightLine4LV7.GetComponent<Renderer>().material = Green;
            LightLine5LV7.GetComponent<Renderer>().material = Green;
            LightLine6LV7.GetComponent<Renderer>().material = Green;
            LightLine7LV7.GetComponent<Renderer>().material = Green;
            LightLine8LV7.GetComponent<Renderer>().material = Green;
            PhotosL7.SetActive(true);
            Frame7.GetComponent<Renderer>().material = Green;
        }
        else if (Nv7 < 2)
        {
            LightLine1LV7.GetComponent<Renderer>().material = Red;
            LightLine2LV7.GetComponent<Renderer>().material = Red;
            LightLine3LV7.GetComponent<Renderer>().material = Red;
            LightLine4LV7.GetComponent<Renderer>().material = Red;
            LightLine5LV7.GetComponent<Renderer>().material = Red;
            LightLine6LV7.GetComponent<Renderer>().material = Red;
            LightLine7LV7.GetComponent<Renderer>().material = Red;
            LightLine8LV7.GetComponent<Renderer>().material = Red;
            PhotosL7.SetActive(false);
            Frame7.GetComponent<Renderer>().material = LightBlue;
        }

        if (Nv8 == 3)
        {
            LightLine1LV8.GetComponent<Renderer>().material = Green;
            LightLine2LV8.GetComponent<Renderer>().material = Green;
            LightLine3LV8.GetComponent<Renderer>().material = Green;
            LightLine4LV8.GetComponent<Renderer>().material = Green;
            LightLine5LV8.GetComponent<Renderer>().material = Green;
            LightLine6LV8.GetComponent<Renderer>().material = Green;
            LightLine7LV8.GetComponent<Renderer>().material = Green;
            PhotosL8.SetActive(true);
            Frame8.GetComponent<Renderer>().material = Green;
        }
        else if (Nv8 < 3)
        {
            LightLine1LV8.GetComponent<Renderer>().material = Red;
            LightLine2LV8.GetComponent<Renderer>().material = Red;
            LightLine3LV8.GetComponent<Renderer>().material = Red;
            LightLine4LV8.GetComponent<Renderer>().material = Red;
            LightLine5LV8.GetComponent<Renderer>().material = Red;
            LightLine6LV8.GetComponent<Renderer>().material = Red;
            LightLine7LV8.GetComponent<Renderer>().material = Red;
            PhotosL8.SetActive(false);
            Frame8.GetComponent<Renderer>().material = LightBlue;
        }

        if (Nv9 == 5)
        {
            LightLine1LV9.GetComponent<Renderer>().material = Green;
            LightLine2LV9.GetComponent<Renderer>().material = Green;
            LightLine3LV9.GetComponent<Renderer>().material = Green;
            LightLine4LV9.GetComponent<Renderer>().material = Green;
            LightLine5LV9.GetComponent<Renderer>().material = Green;
            LightLine6LV9.GetComponent<Renderer>().material = Green;
            LightLine7LV9.GetComponent<Renderer>().material = Green;
            PhotosL9.SetActive(true);
            Frame9.GetComponent<Renderer>().material = Green;
        }
        else if (Nv9 < 5)
        {
            LightLine1LV9.GetComponent<Renderer>().material = Red;
            LightLine2LV9.GetComponent<Renderer>().material = Red;
            LightLine3LV9.GetComponent<Renderer>().material = Red;
            LightLine4LV9.GetComponent<Renderer>().material = Red;
            LightLine5LV9.GetComponent<Renderer>().material = Red;
            LightLine6LV9.GetComponent<Renderer>().material = Red;
            LightLine7LV9.GetComponent<Renderer>().material = Red;
            PhotosL9.SetActive(false);
            Frame9.GetComponent<Renderer>().material = LightBlue;
        }
        if (Nv10 == 4)
        {
            VidriosEstrellas.SetActive(false);
            VidriosSaturno.SetActive(false);
            VidriosLuna.SetActive(false);
            VidriosEarth.SetActive(false);
        }
        if (Nv10 == 8)
        {
            LightLine1LV10.GetComponent<Renderer>().material = Green;
            LightLine2LV10.GetComponent<Renderer>().material = Green;
            LightLine3LV10.GetComponent<Renderer>().material = Green;
            LightLine4LV10.GetComponent<Renderer>().material = Green;
            LightLine5LV10.GetComponent<Renderer>().material = Green;
            LightLine6LV10.GetComponent<Renderer>().material = Green;
            LightLine7LV10.GetComponent<Renderer>().material = Green;
            LightLine8LV10.GetComponent<Renderer>().material = Green;
            PhotosL10.SetActive(true);
            Frame10.GetComponent<Renderer>().material = Green;
        }
        else if (Nv10 < 8)
        {
            LightLine1LV10.GetComponent<Renderer>().material = Red;
            LightLine2LV10.GetComponent<Renderer>().material = Red;
            LightLine3LV10.GetComponent<Renderer>().material = Red;
            LightLine4LV10.GetComponent<Renderer>().material = Red;
            LightLine5LV10.GetComponent<Renderer>().material = Red;
            LightLine6LV10.GetComponent<Renderer>().material = Red;
            LightLine7LV10.GetComponent<Renderer>().material = Red;
            LightLine8LV10.GetComponent<Renderer>().material = Red;
            PhotosL10.SetActive(false);
            Frame10.GetComponent<Renderer>().material = LightBlue;
        }   

        if (bossOpen == false)
        {
            bossOpen = chequearSiBossEstaOpen();
        }

        else
        {
            FinalDoor.SetActive(false);
            PlacaFinal.SetActive(true);
           
        }

    }

    public bool chequearSiBossEstaOpen()
    {
        List<bool> lstFinalBossConditions = new List<bool>();

        lstFinalBossConditions.Add(Nv1 == 3);
        lstFinalBossConditions.Add(Nv2 == 5);
        lstFinalBossConditions.Add(Nv3 == 5);
        lstFinalBossConditions.Add(Nv4 == 8);
        lstFinalBossConditions.Add(Nv5 == 6);
        lstFinalBossConditions.Add(Nv6 == 7);
        lstFinalBossConditions.Add(Nv7 == 2);
        lstFinalBossConditions.Add(Nv8 == 3);
        lstFinalBossConditions.Add(Nv9 == 5);
        lstFinalBossConditions.Add(Nv10 == 8);


        int contador =0;

        foreach (bool condicion in lstFinalBossConditions)
        {
            if (condicion == true)
            {
                contador++;
            }
        }

        if (contador == lstFinalBossConditions.Count)
        {
            return true;
        }

        return false;
    }
}
