using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Pet : MonoBehaviour
{
    public GameObject pantalla;
    public GameObject Pelota, LetraP, LetraE, LetraT;

    private void OnCollisionEnter(Collision collision)
    {
        if (Pelota.tag == "Agarrable")
        {
            pantalla.GetComponent<VideoPlayer>().enabled = true;
            Pelota.GetComponent<BoxCollider>().enabled = false;
            LetraP.SetActive(true);
            LetraE.SetActive(true);
            LetraT.SetActive(true);
        }
    }
   
}
