using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordPress : MonoBehaviour
{
    public char LetraRequerida;
    public int Nivel;
    public GameObject xWordPress;
    public Material Green, LightBlue;
    public AudioSource audioo, audiowrong;





    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Agarrable"))
        {
            Cube cube = collision.gameObject.GetComponent<Cube>();
            
            if (cube.DarLetra() == LetraRequerida)
            {
                xWordPress.GetComponent<Renderer>().material = Green;
                audioo.Play();

                if (Nivel == 1)
                {
                   LvlManager.instance.Lvl1(true);
                   
                }
                else if (Nivel == 2)
                {
                   LvlManager.instance.Lvl2(true);
                    
                }
                else if (Nivel == 3)
                {
                   LvlManager.instance.Lvl3(true);
                   
                }
                else if (Nivel == 4)
                {
                    LvlManager.instance.Lvl4(true);
                    
                }
                else if (Nivel == 5)
                {
                    LvlManager.instance.Lvl5(true);
                   
                }
                else if (Nivel == 6)
                {
                    LvlManager.instance.Lvl6(true);
                    
                }
                else if (Nivel == 7)
                {
                    LvlManager.instance.Lvl7(true);
                    
                }
                else if (Nivel == 8)
                {
                    LvlManager.instance.Lvl8(true);
                  
                }
                else if (Nivel == 9)
                {
                    LvlManager.instance.Lvl9(true);
                  
                }
                else if (Nivel == 10)
                {
                    LvlManager.instance.Lvl10(true);
                  
                }

            }
            else
            {
                audiowrong.Play();
            }
        }
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Agarrable"))
        {
            Cube cube = collision.gameObject.GetComponent<Cube>();

            if (cube.DarLetra() == LetraRequerida)
            {
                xWordPress.GetComponent<Renderer>().material = LightBlue;


                if (Nivel == 1)
                {
                    LvlManager.instance.Lvl1(false);
                }
                else if (Nivel == 2)
                {
                    LvlManager.instance.Lvl2(false);

                }
                else if (Nivel == 3)
                {
                    LvlManager.instance.Lvl3(false);

                }
                else if (Nivel == 4)
                {
                    LvlManager.instance.Lvl4(false);

                }
                else if (Nivel == 5)
                {
                    LvlManager.instance.Lvl5(false);

                }
                else if (Nivel == 6)
                {
                    LvlManager.instance.Lvl6(false);

                }
                else if (Nivel == 7)
                {
                    LvlManager.instance.Lvl7(false);

                }
                else if (Nivel == 8)
                {
                    LvlManager.instance.Lvl8(false);

                }
                else if (Nivel == 9)
                {
                    LvlManager.instance.Lvl9(false);

                }
                else if (Nivel == 10)
                {
                    LvlManager.instance.Lvl10(false);

                }
                

            }
        }

    }



}
